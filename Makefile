CXX=g++
CXXFLAGS=
LDFLAGS=

main: main.o
	$(CXX) $(LDFLAGS) -o $@ $^

main.o: main.cpp
	$(CXX) $(CXXFLAGS) -o $@ -c $^

.PHONY: clean

clean:
	-rm main *.o
