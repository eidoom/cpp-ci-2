# [cpp-ci-2](https://gitlab.com/eidoom/cpp-ci-2)

Trying out some hooks from https://pre-commit.com/hooks.html

* https://github.com/pocc/pre-commit-hooks
* https://gitlab.com/daverona/pre-commit/cpp

Includes programs:
* https://clang.llvm.org/docs/ClangFormatStyleOptions.html
* http://cppcheck.sourceforge.net/
* https://clang.llvm.org/extra/clang-tidy/
* https://oclint.org/
	* Install from https://github.com/oclint/oclint/releases with https://docs.oclint.org/en/stable/intro/installation.html#option-1-directly-adding-to-path

`clang-tidy` and `oclint` first need `bear -- make -B`.

More ideas: https://lefticus.gitbooks.io/cpp-best-practices/content/02-Use_the_Tools_Available.html
